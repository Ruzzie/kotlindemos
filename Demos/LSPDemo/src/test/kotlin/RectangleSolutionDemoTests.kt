import org.assertj.core.api.Assertions.assertThat
import kotlin.test.Test

class RectangleSolutionDemoTests {

    @Test
    fun areaOfShapesTest() {

        var aRectangleShape: Rectangle = Rectangle(3, 4)

        clientMethodThatNeedsAreaOfAShape(aRectangleShape, 12)

        aRectangleShape.height = 5
        aRectangleShape.width = 4

        clientMethodThatNeedsAreaOfAShape(aRectangleShape, 20)

        //Now let's make it a square
        var aSquareShape = Square(4)

        clientMethodThatNeedsAreaOfAShape(aSquareShape, 16)

        //Now use the same validation / asserts as on the original rectangle
        aSquareShape.sideLength = 5
        clientMethodThatNeedsAreaOfAShape(aSquareShape, 25)
    }

    private fun clientMethodThatNeedsAreaOfAShape(aShape: Shape, expectedArea: Int) {

        assertThat(aShape.area()).isEqualTo(expectedArea)
    }

    interface Shape {
        //You could name this, (I) ShapeArea, since this would indicate a certain behavior / property
        fun area(): Int
    }

    class Rectangle(var width: Int, var height: Int) : Shape {
        override fun area(): Int {
            return width * height
        }
    }

    class Square(var sideLength: Int) : Shape {
        override fun area(): Int {
            return sideLength * sideLength
        }
    }
}