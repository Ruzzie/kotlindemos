import kotlin.test.Test
import org.assertj.core.api.Assertions.assertThat


//Kotlin VAL, VAR, collections: emptyList(), listOf()
//Refactoring: rename, move
//Kotlin: class, data class
//Interfaces
//properties

class MyTests {
    @Test
    fun oneAndTwoIsThree() {
        //Arrange
        val calculator = Calculator()
        //Act
        val sum = calculator.Sum(1, 2)
        //Assert
        assertThat(sum).isEqualTo(3)
    }

    @Test
    fun tenMinusHundredIsNegative() {
        //Arrange
        val calculator = Calculator()

        //Act
        val sum = calculator.Sum(10, -100)

        //Assert
        assertThat(sum).isLessThan(0)
    }
}

class Calculator {
    fun Sum(a: Int, b: Int): Int {
        return a + b
    }
}
