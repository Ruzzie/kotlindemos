import org.assertj.core.api.Assertions.assertThat
import kotlin.test.Test

class PolymorphismDemoTests {

    @Test
    fun abstractClassInheritance() {
        //When you think of OO Design and try to model the real world in Code, there can be a mismatch,
        // amongst others this is addresses in the OCP, LSP and the ISP.
        //In this example we will use a simple real world model

        val myTestCar : Car = Car()

        myTestCar.drive()
        //val v = Vehicle() //Cannot Instantiate abstract class

        val aBikeVehicleObject: Vehicle = Bike()
        val aCarVehicleObject: Vehicle = Car()

        //Since bike and car are defined as Vehicle
        //we only have access to the methods and properties
        // of their base type Vehicle
        aBikeVehicleObject.drive()
        //aBikeVehicleObject.doWheelie() //cannot access this through base type
        aCarVehicleObject.drive()
        //aCarVehicleObject.drift() //cannot access this through base type

        val myBike = Bike()
        myBike.drive()
        myBike.doWheelie()

        val myCar = Car()
        myCar.drive()
        myCar.drift()

        fun vehicleDriveTester(vehicle: Vehicle) {
            vehicle.drive()
            assertThat(vehicle.isDriving).isTrue()
        }

        vehicleDriveTester(myBike)
        vehicleDriveTester(myCar)

        //Let't change the drive to abstract and see what happens
    }

    //Abstract class can define default implementation and 'abstract' methods and properties
    abstract class Vehicle {

        var isDriving: Boolean = false
            protected set

        fun drive() {
            isDriving = true
        }
    }

    class Car : Vehicle() {
        var isDrifting: Boolean = false

        fun drift() {
            isDrifting = true
        }
    }

    class Bike : Vehicle() {

        var isPerformingWheelie: Boolean = false

        fun doWheelie() {
            isPerformingWheelie = true
        }
    }
}