import junit.framework.Assert.assertEquals
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.given
import org.jetbrains.spek.api.dsl.it
import org.jetbrains.spek.api.dsl.on
import org.assertj.core.api.Assertions.assertThat
import org.jetbrains.spek.api.dsl.describe

//Preferred Spek style: Given: On : It
object CalculatorSpec: Spek({
    given("a calculator") {
        val calculator = SampleCalculator()
        on("addition") {
            val sum = calculator.sum(2, 4)
            it("should return the result of adding the first number to the second number") {
                assertThat(sum).isEqualTo(6)
                assertEquals(6, sum)

                "MyString".map { c ->  c}
                "MyString".associateBy({t -> t})
            }
        }
        on("subtraction") {
            val subtract = calculator.subtract(4, 2)
            it("should return the result of subtracting the second number from the first number") {
                assertThat(subtract).isEqualTo(2)
                assertEquals(2, subtract)
            }
        }
    }
})

//Alternative style: describe, on, it
object SimpleSpec: Spek({
    describe("a calculator") {
        val calculator = SampleCalculator()

        on("addition") {
            val sum = calculator.sum(2, 4)

            it("should return the result of adding the first number to the second number") {
                assertEquals(6, sum)
            }
        }

        on("subtraction") {
            val subtract = calculator.subtract(4, 2)

            it("should return the result of subtracting the second number from the first number") {
                assertEquals(2, subtract)
            }
        }
    }
})


class SampleCalculator {
    fun sum(a: Int, b: Int): Int {
        return a+b
    }

    fun subtract(a: Int, b: Int): Int {
        return a-b
    }

}
