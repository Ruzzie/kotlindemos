import kotlin.test.Test
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.groups.Tuple
import java.time.LocalDate

class UserSessionContainerTest {

    @Test
    fun isSameInstance() {

        //Arrange
        //Act
        val instanceA = UserSessionContainer;
        val instanceB = UserSessionContainer;

        //Call something
        val session = instanceA.createOrGetSession(1, "name")

        //Assert
        assertThat(instanceA).isEqualTo(instanceB)
    }

    @Test
    fun createOrGetShouldCreateNewUserSessionWithParametersSet() {
        //Arrange
        //Act
        val userSession = UserSessionContainer.createOrGetSession(1, "Doctor Who?!")

        //Assert
        assertThat(userSession.sessionId).isEqualTo(1);
        assertThat(userSession.userName).isEqualTo("Doctor Who?!");
    }

    @Test
    fun createOrGetShouldReturnSameObjectWhenCalledTwiceWithTheSameParameters() {

        //Arrange
        val userSessionNew = UserSessionContainer.createOrGetSession(2, "User Two")

        //Act
        val sameUserSession = UserSessionContainer.createOrGetSession(2, "User Two")

        //Assert
        assertThat(userSessionNew).isEqualTo(sameUserSession);
    }

    @Test
    fun destroySessionShouldDecrementCounter() {
        //Arrange
        val userSession = UserSessionContainer.createOrGetSession(65214, "Destroy me!")
        val oldCounterValue = UserSessionContainer.SessionCounter;

        //Act
        UserSessionContainer.destroySession(userSession)
        val newCounterValue = UserSessionContainer.SessionCounter

        //Assert
        assertThat(newCounterValue).isLessThan(oldCounterValue)


        val OneTwoThree = "123"
        val FourFiveSix = "456"
        val (b, c) = pairStrings(OneTwoThree, FourFiveSix)

        val d = b+c
    }

    private fun pairStrings(OneTwoThree: String, FourFiveSix: String): Pair<String, String> {
        val b = OneTwoThree + FourFiveSix
        val c = FourFiveSix + OneTwoThree
        return Pair(b, c)
    }

    @Test
    fun sessionCounterShouldIncrementWhenAddingNewSession() {
        //Arrange
        val oldCounterValue = UserSessionContainer.SessionCounter;

        //Act
        val userSession = UserSessionContainer.createOrGetSession(991, "Count Should increase!")
        val newCounterValue = UserSessionContainer.SessionCounter

        //Assert
        assertThat(newCounterValue).isGreaterThan(oldCounterValue);
    }

    @Test
    fun destroySessionShouldRemoveUserSession() {
        //Arrange
        val userSession = UserSessionContainer.createOrGetSession(67, "destroySessionShouldRemoveUserSession")

        //Act
        UserSessionContainer.destroySession(userSession)

        //Assert
        val differentSession = UserSessionContainer.createOrGetSession(67, "destroySessionShouldRemoveUserSession")
        assertThat(differentSession).isNotEqualTo(userSession)
    }
}

//Singleton
object UserSessionContainer {
    var SessionCounter: Int = 0
        private set

    private val userSessionsMap: MutableMap<Tuple, UserSession> = mutableMapOf<Tuple, UserSession>()

    init {
        //initialization, called only once
       // userSessionsMap
    }

    fun createOrGetSession(sessionId: Int, userName: String): UserSession {
        val key = Tuple(sessionId, userName)

        //create new session or return existing session
        SessionCounter++
        return userSessionsMap.getOrPut(key, defaultValue = { UserSession(sessionId, userName, LocalDate.now()) })
    }

    fun destroySession(userSession: UserSession) {
        //destroys session
        SessionCounter--
        val key = Tuple(userSession.sessionId, userSession.userName)

        userSessionsMap.remove(key)
    }
}

//decouples with companion object
//class UserSessionContainer {
//    companion object {
//        var SessionCounter: Int = 0
//            private set
//
//        init {
//            //initialization, called only once
//        }
//
//        fun createOrGetSession(sessionId: Int, userName: String): UserSession {
//            //create new session or return existing session
//            return UserSession(sessionId, userName, LocalDate.now())
//        }
//
//        fun destroySession(userSession: UserSession) {
//            //destroys session
//        }
//    }
//}

class UserSession(val sessionId: Int, val userName: String, val startTime: LocalDate)