//package <default> (D:\Users\Dorus\IdeaProjects\MtgCardReader\ConsoleApplication\src\test\resources)
//
//import com.fasterxml.jackson.annotation.JsonProperty
//
//data class Card(@JsonProperty("ColorIdentity")
//                val colorIdentity: Int = 0,
//                @JsonProperty("Types")
//                val types: String = "",
//                @JsonProperty("BasicType")
//                val basicType: Int = 0,
//                @JsonProperty("MultiverseId")
//                val multiverseId: Int = 0,
//                @JsonProperty("Price")
//                val price: Double = 0.0,
//                @JsonProperty("Rating")
//                val rating: Int = 0.0,
//                @JsonProperty("ManaCost")
//                val manaCost: String? = "",
//                @JsonProperty("Cmc")
//                val cmc: Int = 0,
//                @JsonProperty("ImageRelativeUrl")
//                val imageRelativeUrl: String = "",
//                @JsonProperty("Legality")
//                val legality: Int = 0,
//                @JsonProperty("Name")
//                val name: String = "")