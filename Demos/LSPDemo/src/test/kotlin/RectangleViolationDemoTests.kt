import org.assertj.core.api.Assertions.assertThat
import kotlin.test.Test

class RectangleViolationDemoTests {

    @Test
    fun areaOfRectanglesTest() {

        var aRectangle = Rectangle(3, 4)

        assertThat(aRectangle.area()).isEqualTo(12)

        clientMethodThatDoesSomethingWithARectangle(aRectangle)

        //Now let's make it a square
        aRectangle = Square(4)
        assertThat(aRectangle.area()).isEqualTo(16)

        //Now use the same validation / asserts as on the original rectangle
        clientMethodThatDoesSomethingWithARectangle(aRectangle) // this fails

        //This violates the LSP, the Subtype Square cannot be substituted for a Rectangle in ALL cases
    }

    private fun clientMethodThatDoesSomethingWithARectangle(aRectangle : Rectangle){
        aRectangle.height = 5
        aRectangle.width = 4

        assertThat(aRectangle.area()).isEqualTo(20)
    }

    open class Rectangle(open var width: Int, open var height: Int) {

        fun area(): Int {
            return width * height;
        }
    }

    class Square(sideLength: Int) : Rectangle(sideLength, sideLength) {

        override var width = sideLength
            set (value) {
                super.width = value
                super.height = value
            }

        override var height = sideLength
            set (value) {
                super.width = value
                super.height = value
            }
    }
}