import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.result.Result
import org.assertj.core.api.Assertions.assertThat
import java.io.File
import kotlin.test.Test

class SmokeTests {

    @Test
    fun downloadFileFromUrlTest() {

        //Arrange
        val url = "https://ruzziemtgstorage.blob.core.windows.net/demo/allcards.json"
        var jsonString = ""

        val (_, _, result) = url.httpGet().responseString()

        if (result is Result.Success) {
            //trim of bom, json serializer doesn't like that
            jsonString = result.get().trim('\ufeff')
        }

        File("filename.ext").printWriter().use { out ->
            out.print("data to write")
        }

        //Deserialize
        val json = jacksonObjectMapper()
        var allCards: List<Card> = json.readValue(jsonString)

        //Assert
        assertThat(allCards).hasSize(17449)
    }
}

data class Card(@JsonProperty("ColorIdentity")
                val colorIdentity: Int = 0,
                @JsonProperty("Types")
                val types: String = "",
                @JsonProperty("BasicType")
                val basicType: Int = 0,
                @JsonProperty("MultiverseId")
                val multiverseId: Int = 0,
                @JsonProperty("Price")
                val price: Double = 0.0,
                @JsonProperty("Rating")
                val rating: Double = 0.0,
                @JsonProperty("ManaCost")
                val manaCost: String? = "",
                @JsonProperty("Cmc")
                val cmc: Int = 0,
                @JsonProperty("ImageRelativeUrl")
                val imageRelativeUrl: String? = "",
                @JsonProperty("Legality")
                val legality: Int = 0,
                @JsonProperty("Name")
                val name: String = "")